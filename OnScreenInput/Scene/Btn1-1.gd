extends TextureButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var key : int = -2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func on_button_pressed(key_id):		
	if key_id.has(key):
		set_pressed_no_signal(true)
		$KeyText.add_color_override("font_color", Color.aliceblue)
	else:
		set_pressed_no_signal(false)
		$KeyText.add_color_override("font_color",Color.white)


func get_class():
	return "KeyCap"
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


