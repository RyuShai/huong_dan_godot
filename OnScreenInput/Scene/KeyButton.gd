extends Control

var KeyCapText = ["esc", "F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","PrS","Pse","SLK",
				"`~","1!","2@","3#","4$","5%","6^","7&","8*","9(","0)","-_","=+","Back","Irt","HoM","PUp",
				"Tab","q","w","e","r","t","y","u","i","o","p","[{","]}","\\|","Del","End","PDw",
				"Capk","a","s","d","f","g","h","j","k","l",";:","'\"","Enter",
				"Shift","z","x","c","v","b","n","m",",<",".>","/?","shift","↑",
				"Ctl","Win","Alt","","Alt","Win","fn","Ctl","←","↓","→"
				]
var KetCapValue = [0x1B, 0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x2C,0x13,0x91,
				0xC0,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x60,0xBD,0xBB,0x08,0x2D,0x24,0x21,
				0x09,0x51,0x57,0x45,0x52,0x54,0x59,0x55,0x49,0x4F,0x50,0xDB,0xDD,0xDC,0x2E,0x23,0x22,
				0x14,0x41,0x53,0x44,0x46,0x47,0x48,0x4A,0x4B,0x4C,0xBA,0xDE,0x0D,
				0x10,0x5A,0x58,0x43,0x56,0x42,0x4E,0x4D,0xBC,0xBE,0xBF,-0x10,0x26,
				0x11,0x5B,0x12,0x20,0x5D,0x5C,-0x02,-0x11,0x25,0x28,0x27]
var currentIndex = 0
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

onready var keyTrack = preload("res://NativeLib/trackKey.gdns").new()

signal key_pressed(key_id)
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#test
	setTextForKeyCap(self)
	#endtest
	get_tree().get_root().set_transparent_background(true)
	$Timer.connect("timeout",self,"onTimeOut")
	
	keyTrack.connect("keyPressed",self, "onKeyPressed" )
	
	$Timer.start()
	pass # Replace with function body.

func onKeyPressed(node, key_id : Array):
#	print("singal key id: ", key_id , node.name)
#	print("typeof: ", typeof(key_id))
	emit_signal("key_pressed",key_id)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass

func onTimeOut():
#	print("time out")
	keyTrack.getKeyPressed()

func setTextForKeyCap(node):
	if currentIndex == KeyCapText.size():
		return
	if node.get_child_count() >0 :
		if node.get_class() != "KeyCap":
			var count = node.get_child_count()
			for i in range (count):
				setTextForKeyCap(node.get_child(i))
		else:
			(node.get_child(0) as Label).set_text(KeyCapText[currentIndex])
			node.key = KetCapValue[currentIndex]
			self.connect("key_pressed",node,"on_button_pressed")
			currentIndex+=1;
	pass




func _on_Control_mouse_entered():
	print("enter")
	if OS.window_borderless:
		OS.set_borderless_window(false) 
	pass # Replace with function body.


func _on_Control_mouse_exited():
	print("exit")
	if !OS.window_borderless:
		OS.set_borderless_window(true) 
	pass # Replace with function body.
