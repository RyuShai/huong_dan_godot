#ifndef KEYTRACK_H
#define KEYTRACK_H

#include <Godot.hpp>
#include <Reference.hpp>
#include <Node.hpp>
#include <core/Array.hpp>
#include <windows.h>
#include <winuser.h>
using namespace godot;
class KeyTrack : public Node
{
    GODOT_CLASS(KeyTrack, Node)
public:
    KeyTrack();
    static void _register_methods();
    void getKeyPressed();
    void _init();
};

#endif // KEYTRACK_H
