#include "keytrack.h"
#include <vector>
#pragma comment(lib, "User32.lib")
KeyTrack::KeyTrack()
{
}

void KeyTrack::_register_methods()
{
    register_method("getKeyPressed", &KeyTrack::getKeyPressed);

    register_signal<KeyTrack>((char*)"keyPressed","node", GODOT_VARIANT_TYPE_OBJECT, "key_id", GODOT_VARIANT_TYPE_ARRAY);
}

void KeyTrack::getKeyPressed()
{
    godot::Array array;
   for(int i=0 ; i<255; i++){

       if ( GetAsyncKeyState(i) & 0x100000){
           array.push_back(i);
       }
   }
   emit_signal("keyPressed",this, array);
}

void KeyTrack::_init()
{

}


extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);

    godot::register_class<KeyTrack>();
}
