extends HBoxContainer

export (Array, Texture) var items = []
export var speed : float = 700
export var stopTime : int = 5 # 5s
export var speedDownScale : float  = 0.8 #giam 20% so voi toc do ban dau
#
var originPositionY : int;
var isRoll : bool = false
var countTime : float = 0;
var speedOrigin
#
onready var rng = RandomNumberGenerator.new()
#
func _ready():
	rng.randomize()
	originPositionY = $ListItemFirst.rect_position.y;
	speedOrigin = speed
	pass 
#
#
func _process(delta):
	if isRoll:
		for list in get_tree().get_nodes_in_group("ListItem"):
			list.rect_position.y += delta * speed
			if list.rect_position.y >= 61:
				list.rect_position.y = originPositionY
###
###				# load new texture
				var pos = rng.randi()%4
#
				list.get_child(1).texture = items[pos]
				list.move_child(list.get_child(1),0)

#
	pass
#
func _on_Button_pressed():
	print ("button clicked")
	isRoll = true
	$StopRolling.start()
	pass # Replace with function body.

#
func _on_StopRolling_timeout():
	speed  = speed*speedDownScale
	countTime+= $StopRolling.wait_time;
	print("count time: ", countTime)
	if(countTime >= stopTime):
		isRoll = false
		$StopRolling.stop()
		
		for list in get_tree().get_nodes_in_group("ListItem"):
			list.move_child(list.get_child(1),0)
			list.rect_position.y = originPositionY
		
		
		speed = speedOrigin
		countTime = 0
	pass # Replace with function body.
