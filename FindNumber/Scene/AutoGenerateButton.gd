tool
extends GridContainer

export var tableName : String

signal number_correct()

onready var buttonGenerator = preload("res://Scene/NumberButton.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(100):
		var newButton = buttonGenerator.instance() 
		var textValue
		if i <10:
			textValue = "0"+ String(i)
		else:
			textValue = String(i)
		newButton.get_child(0).text = ("0" if GlobalRandomNumber.tableValue[i] <10 else "") + String(GlobalRandomNumber.tableValue[i])
		newButton.connect("number_button_press",self, "on_number_button_press")
		self.connect("number_correct", newButton, "on_number_correct")
		self.add_child(newButton)
	pass # Replace with function body.

func on_number_button_press(number):
	print ("button ",number, " da duoc press" )
	#so sanh number nhan duoc voi GlobalRandomNumber.target
	if int(number) == GlobalRandomNumber.target:
		print ("number == target")
		GlobalRandomNumber.target = GlobalRandomNumber.target+1
		get_parent().get_node("Target").text = String(GlobalRandomNumber.target)
		emit_signal("number_correct",number)
		get_parent().call_update_correct_number(number, tableName)
	pass
	
func on_update_correct_number(number, tableName):
	print("on_update_correct_number")
	for i in range(self.get_child_count()):
		if self.get_child(i).get_node("NumberText").text == number:
			print ("number text == number ")
			if tableName == "red":
				self.get_child(i).texture_normal = load("res://Material/Texture/g3038.png")
			elif tableName == "green":
				self.get_child(i).texture_normal = load("res://Material/Texture/g3044.png")
