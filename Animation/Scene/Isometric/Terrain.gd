extends Node2D

var Brick = preload("res://Scene/Isometric/Brick.tscn")
var width = 720
var height = 720
var terrain : Node2D
var childCount = 0
func _ready():
	width = Brick.instance().texture.get_width()
	height = Brick.instance().texture.get_height()
	print("widht: ", width, "height: ", height)
	terrain = $Terrain
	for i in range(16):
		for j in range(16):
			generate_terrain(i,j)
	pass
	
	
func generate_terrain(var x : int, var y :int):
	var brick = Brick.instance()
	var a =  0.5 * width
	var b = (-0.5)*width
	var posX = x*a + y*b
	
	var c =  0.25 * height
	var d =   0.25 * height
	var posY =  x *c + y *d
	brick.a = a
	brick.b = b
	brick.c = c
	brick.d = d
	brick.x = x
	brick.y = y
	
	brick.position = Vector2(posX,posY)
	terrain.add_child(brick)
	


func _on_Timer_timeout():
	var childrent = terrain.get_children()
	for child in childrent:
		child.change()
	pass # Replace with function body.
